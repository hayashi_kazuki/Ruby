# 1曲分の音楽データのクラスを作る
class MusicInfo
  # MusicInfoクラスのインスタンスを初期化する
  def initialize(id, title, artist, writer, composer, album, genre, time, comment, feeling, code, iswc, music_date )
    @id = id
    @title = title
    @artist = artist
    @writer = writer
    @composer = composer
    @album = album
    @genre = genre
    @time = time
    @comment = comment
    @feeling = feeling
    @code = code
    @iswc = iswc
    @music_date = music_date
  end

  # 最初に検討する属性に対するアクセサを提供する
  attr_accessor :id, :title, :artist, :writer, :composer, :album, :genre, :time, :comment, :feeling, :code, :iswc, :music_date

  # MusicInfoクラスのインスタンスをCSV形式へ変換する
  def to_csv( id )
    "#{@id},#{@title},#{@artist},#{@writer},#{@composer},#{@album},#{@genre},#{@time},#{@comment},#{@feeling},#{@code},#{@iswc},#{@music_date}\n"
  end

  # MusicInfoクラスのインスタンスの文字列表現を返す
  def to_s
    "#{@id}, #{@title}, #{@artist}, #{@writer}, #{@composer}, #{@album}, #{@genre}, #{@time}, #{@comment}, #{@feeling}, #{@code}, #{@iswc}, #{@music_date}"
  end

  # 蔵書データを書式をつけて出力する操作を追加する
  # 項目の区切り文字を引数に指定することができる
  # 引数を省力した場合は改行を区切り文字にする
  def to_formatted_string( sep = "\n" )
    "ID: #{@id}#{sep}曲名: #{@title}#{sep}アーティスト名: #{@artist}#{sep}作詞者: #{@writer}#{sep}作曲者: #{@composer}#{sep}アルバム名: #{@album}#{sep}ジャンル: #{@genre}#{sep}曲の長さ: #{@time}#{sep}コメント: #{@comment}#{sep}気分: #{@feeling}#{sep}作品コード: #{@code}#{sep}ISWC: #{@iswc}#{sep}発売日: #{@music_date}#{sep}"
  end
end
  
