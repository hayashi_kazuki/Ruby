require "date"
require "./music_info.rb"
require "./traffic.rb"
require "./your.rb"
require "./first.rb"

# MusicInfoManagerクラスを定義する
class MusicInfoManager
  include Traffic
  include Your
  include First
  def initialize( filename )
    # 初期化でcsvファイルを指定する
    @csv_fname = filename
    # 音楽データのハッシュ
    @music_infos = {}
  end

# 音楽データをセットアップする
  def set_up
    # csvファイルを読み込みモードでオープンする
    open(@csv_fname, "r:UTF-8") {|file|
    # ファイルの1行ずつ取り出して、lineに読み込む
    file.each {|line|
      # lineからchompで改行を取り除き、splitでカンマ区切りに分割し、
      # 左辺のそれぞれの項目へ多重代入する
      id,title,artist,writer,composer,album,genre,time,comment,feeling,code,iswc,mdate = line.chomp.split(',')
      # 音楽データの1件分のインスタンスを作成してハッシュに登録する
      # strptimeは、文字列からDateクラスのインスタンスを作成するメソッド
      @music_infos[ id ] =
        MusicInfo.new(id, title, artist, writer, composer, album, genre, time, comment, feeling, code, iswc, Date.strptime(mdate))
    } # 1行ずつの処理の終わり
  } # ファイルを閉じている
  end

# 音楽データを登録する
  def add_music_info
    puts "項目に入力してください"
    # 音楽データ1件分のインスタンスを作成する
    music_info = MusicInfo.new("", "", "", "", "", "", "", "", "", "", "", "", Date.new)
    # 登録するデータを項目ごとに入力する

    begin

    print "\n"
    print "ID: "
    music_info.id = gets.chomp
    print "曲名: "
    music_info.title = gets.chomp
    print "アーティスト名: "
    music_info.artist = gets.chomp
    print "作詞: "
    music_info.writer = gets.chomp
    print "作曲: "
    music_info.composer = gets.chomp
    print "アルバム名: "
    music_info.album = gets.chomp
    print "ジャンル: "
    music_info.genre = gets.chomp
    print "曲の長さ: "
    music_info.time = gets.chomp
    print "コメント: "
    music_info.comment = gets.chomp
    print "気分: "
    music_info.feeling = gets.chomp
    print "作品コード: "
    music_info.code = gets.chomp
    print "ISWC: "
    music_info.iswc = gets.chomp
    print "発売年: "
    year = gets.chomp.to_i
    print "発売月: "
    month = gets.chomp.to_i
    print "発売日: "
    day = gets.chomp.to_i
    music_info.music_date = Date.new( year, month, day )
 
    # 作成した音楽データを1件分をハッシュに登録する
    @music_infos[music_info.id] = music_info
    # 自動保存する
    save_all_music_infos
    puts "\n1件登録しました"
    
    rescue
      puts "-----------------"
      puts "入力が不十分です"
      gets
    end
  end

  # 音楽データの一覧を表示する
  def list_all_music_infos
    puts "\n--------------------" 
    @music_infos.each { |id, info|
     print info.to_formatted_string
    puts "\n--------------------"
    }
  end

  # 音楽検索
  def search_music_info
    puts "検索したいデータのIDを入力してください"
    print "ID: "
    id = gets.chomp
    puts "\n～検索結果～"
    puts "\n"
      # valuesは配列のすべての値を返す
    info = @music_infos.values_at(id)[0]
    if info != nil
      puts info.to_formatted_string
    else
      puts "データが見つかりませんでした"
    end
  end

  # 気分検索
  def feeling_music_info
    # feelingの一覧を表示させるためにArrayクラスのuniqを使う
    # mapは要素の数だけ繰り返しブロックを実行し、ブロックの戻り値を集めた配列を作成して返す。
      feeling_list = @music_infos.map do |key,info|
      info.feeling
    end
    feeling_hash = {}
    # uniqは配列の中で重複する要素を削除し、新しい配列を返す
    feeling_list.uniq.each_with_index do |feeling,idx|
      feeling_hash[idx+1] = feeling
    end
    feeling_hash.each do |key,value|
      puts "#{key}. #{value}"
    end
    # ユーザに気分を入力してもらう
    print "どんな気分ですか？数字で入力してください: "
    feeling_num = gets.chomp.to_i

    # 入力値と一致するMusic_infoのデータを表示する
    puts "\n--------------------"
    @music_infos.each { |id, info|
      if feeling_hash[feeling_num] == info.feeling
        print info.to_formatted_string
        puts "\n--------------------"
      end 
    }
    
  end

    # 音楽データの削除
    def del_music_info
    # ユーザーに削除対象のIDの入力を促す 
    puts "削除対象のIDを入力してください"
    print "ID: "
    id = gets.chomp
=begin
    open(@csv_fname, "r:UTF-8") {|file|
      file.each {|line|
        id, title, artist, ,writer, composer, album, genre, time, comment, feeling, code, iswc, mdate, = line.chomp.split(',')
        @music_infos[ key ] = MusicInfo.new(title,artist,album,genre,time,Date.strptime(mdate),comment,feeling)          
        } 
      } 
=end
    @music_infos.delete(id)
    save_all_music_infos
    puts "\n削除しました"
  end

  # カラオケ機能
  def karaoke_music_infos
    # I登録されている曲を一覧表示
    list_all_music_infos 
    # ユーザーにIDの入力を促す
    puts "どの曲を歌いますか？IDを入力してください"
    print "ID: "
    num = gets.chomp
    puts "音源を用意してください"
    puts "yの後にエンターを押し、同時に音楽を再生してください\n"
    yesno = gets.chomp.upcase
      if /^Y$/ =~ yesno
        # Yが1文字の時だけ歌詞が出力される
      end
    puts "\n"
   # サブスレッド
   # ユーザーが数字を入力すると歌詞が出力される 
    show_karaoke_thread = Thread.new(num) do |id|
      case id
      when '1'
        traffic
      when '2'
        your
      when '3'
        first
      end
    end 
    
    # サブスレッド
    # ユーザーがエンターを押すとスレッドがkillされる
    stop_karaoke_thread = Thread.new(show_karaoke_thread) do |th|
      gets
      Thread.kill(th)
    end
    # メインスレッドはjoinで待機
    show_karaoke_thread.join
  end

  # 音楽データをファイルへ書き込んで保存する
  def save_all_music_infos
    # csvファイルを書き込みモードでオープンする
    open(@csv_fname, "w:UTF-8") {|file|
      @music_infos.each{ |id, info|
        file.print(info.to_csv( id ))
      }
  # 1行ずつの処理の終わり
    }
  end

  # 処理の選択と選択後の処理を繰り返す
  def run
    while true
      # 機能選択画面を表示する
      print "
1.音楽データの登録
2.音楽データの一覧
3.音楽検索
4.気分検索
5.カラオケ機能
6.音楽データの削除
0.終了
番号を選んでください(1.2.3.4.5.6.0): "

      # 文字の入力を待つ
      num = gets.chomp
      case num
      when '1' 
        # 音楽データの登録
        add_music_info
      when '2'
        # 音楽データの一覧
       list_all_music_infos
      when '3'
        # 音楽検索
        search_music_info
      when '4'
        # 気分検索
        feeling_music_info
      when '5'
        # カラオケ機能
        karaoke_music_infos
      when '6'
        # 音楽データの削除
        del_music_info
      when '0'
        # 終了
        break;
      else
        # 処理待ち選択画面に戻る
      end
    end
  end

end

# ここからがアプリケーションを動かす本体

# アプリケーションのインスタンスを作成する
# 音楽データのcsvファイルを指定している
music_info_manager = MusicInfoManager.new("データベース入力用エクセル.csv")

# MusicInfoManagerの音楽データをセットアップする
music_info_manager.set_up

# MusicInfoManagerの処理の選択と選択後の処理を繰り返す
music_info_manager.run
