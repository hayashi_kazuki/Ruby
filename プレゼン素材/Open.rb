

@map = [[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 3, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]]

        @map_tile = []
        @map_tile[0] = Image.new(32, 32) #背景１（空） 
        @map_tile[1] = Image.load("BK3.PNG") #障害物（ブロック）
        @map_tile[2] = Image.new(32, 32, [0xff,0xff,0xff]) #背景２（雲）
        @map_tile[3] = Image.load("BK2.jpg")#仕掛けブロ
        #描画用RenderTarget
        @target = RenderTarget.new(840,480)

         #背景
        @bg_img = Image.load("bg.png")

        #キャラ
        @char_object = Sprite.new(0, 0, Image.load("player_dora3.png"))
        @char_tile = Image.load("player_dora3.png")
    
        sound = Sound.new("BGM1.wav")

#ゴール
        @warp = Image.load("doko_door.jpg") #senni

        


#フレーム数設定
Window.fps = 60

#スコア設定
font = Font.new(32)
score = ().to_i

#初期値設定
x = 10
y = y_prev = 10
f = 1
can_jump = false

#対応する配列を返す
def collision_tile(x, y, arr)
  return arr[y/32][x/32] #マップ配列の仕様上、ｘとｙが逆になっているのに注意
end

#ゲームループ
Window.loop do

if Input.key_push?(K_B) then
    sound.play
end


  #ジャンプ
if Input.key_push?(K_SPACE) and can_jump
  f = -15
end

  #Ｙ軸移動増分の設定
 y_move = (y - y_prev) + f
 y_prev = y
 #座標増分を加算。増分が31を越えていれば強制的に31とする
y += y_move <= 31 ? y_move : 31
f = 1 #f値を初期化し直す


can_jump = false
  #床衝突判定
if    collision_tile(x   , y+31, @map) == 1 or 
      collision_tile(x+31, y+31, @map) == 1
 y = y/32*32
  can_jump = true #接地しているのでジャンプを許可する
  #天井衝突判定
elsif collision_tile(x   , y   , @map) == 1 or 
      collision_tile(x+31, y   , @map) == 1
  y = y/32*32 + 32
end

  #左右移動
x += Input.x * 2

  #壁衝突判定（左側）
if    collision_tile(x   , y   , @map) == 1 or 
      collision_tile(x   , y+31, @map) == 1
  x = x/32*32 + 32
  #壁衝突判定（右側）
 elsif collision_tile(x+31, y   , @map) == 1 or 
       collision_tile(x+31, y+31, @map) == 1 
   x = x/32*32
 end

#画面端であればオフセットを固定し、そうでなければオフセットを設定する
 case x
 when 0..128-1 #画面左端
 
when 128..575-1
   @target.ox = x - 128
 when 575..640 #画面右端
   @target.ox = 575-128
 end
 case y
 when 0..64-1 #画面上端
   @target.oy = 0

 end

 #スコア加算
 #Sif 
 #object = @char_tile.x === 1000
 #     @char_tile.y === 350
 #if   @char_object == object
 #     switch_to "select.rb"
#  end

if Input.key_push?(K_Z) then
    switch_to "select.rb"
end

  #背景の表示
  @target.draw(0, 0, @bg_img)
  #マップの表示
  @target.draw_tile(nil,nil,@map,@map_tile,nil,nil,nil,nil)
  #ゴールの表示
  @target.draw(1000, 350,@warp)
  #キャラの表示

  @target.draw(x-20, y-40, @char_tile)
  Window.draw(0, 0, @target)
  Window.draw_font(50, 0, "Ruby学び隊戦闘力:10", font)
end