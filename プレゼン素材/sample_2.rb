#require 'dxruby'
#font = Font.new(32)
#Window.loop do
#  Window.draw_font(30, 30, "Hello World", font)
#end
#UTF-8
require "dxruby"
require "scene_switcher"

image = Image.load("images.jpg")  # data.pngを読み込む
sound = Sound.new("opning.wav")

Window.loop do
  Window.draw(150, 150, image)  # data.pngを描画する
  if Input.key_push?(K_Z) then
    sound.play
  end

  if Input.key_push?(K_C) then
    switch_to "Open.rb"
  end
end

#if $0 == __FILE__
# Open.new
#end