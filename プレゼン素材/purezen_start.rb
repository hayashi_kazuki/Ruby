require 'dxruby'
 
#
# パイプ内のような背景描画を行う
#
#class PipeBg
  @@hlsl = <<EOS
  float sz;
  float r;
  float angle_max;
  float start_x;
  float start_y;
  texture tex0;
 
  sampler Samp = sampler_state {
    Texture =<tex0>;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    MipFilter = LINEAR;
    // MinFilter = POINT;
    // MagFilter = POINT;
    // MipFilter = POINT;
    AddressU = BORDER;
    AddressV = WRAP;
  };
 
  float4 PS(float2 input : TEXCOORD0) : COLOR0
  {
    input.x = (input.x - 0.5) * (r * cos(atan2(input.y - 0.5, sz))) / sz + start_x + 0.5;
    input.y = (atan2(input.y - 0.5, sz) / angle_max) + start_y;
    return tex2D( Samp, input );
  }
 
  technique {
    pass {
      PixelShader = compile ps_2_0 PS();
    }
  }
EOS
 
  attr_accessor :sz
  attr_accessor :r
  attr_accessor :angle_max
  attr_accessor :start_x
  attr_accessor :start_y
  attr_accessor :shader
 
  #
  # コンストラクタ
  #
  # @param [Number] sz スクリーンまでの距離
  # @param [Number] r 壁までの距離
  # @param [Number] start_x 初期位置 x
  # @param [Number] start_y 初期位置 y
  #
  def initialize(sz = 160, r = 280, start_x = 0.0, start_y = 0.0)
    self.sz = sz
    self.r = r
    self.start_x = start_x
    self.start_y = start_y
 
    core = Shader::Core.new(@@hlsl,{
                              :sz=>:float,
                              :r=>:float,
                              :angle_max=>:float,
                              :start_x=>:float,
                              :start_y=>:float
                            })
    self.shader = Shader.new(core)
    self.shader.sz = self.sz.to_f / (Window.height / 2)
    self.shader.r = self.r.to_f / (Window.width / 2)
    self.shader.start_x = self.start_x
    self.shader.start_y = self.start_y
    self.angle_max = Math.atan2(Window.height / 2, sz)
    # self.angle_max = 90.0 * Math::PI / 180.0
    self.shader.angle_max = self.angle_max
  end
 
  #
  # 描画開始位置を更新
  #
  # @param [Number] dx テクスチャ取得位置 x 増分(1.0より小さい値が望ましい)
  # @param [Number] dy テクスチャ取得位置 y 増分(1.0より小さい値が望ましい)
  #
  def update(dx, dy)
    self.start_x += dx
    self.start_y += dy
    self.shader.start_x = self.start_x
    self.shader.start_y = self.start_y
  end
 
  #
  # 描画
  #
  # @param [Number] x 描画位置 x
  # @param [Number] y 描画位置 y
  #
  def draw(x, y, img)
    Window.drawEx(x, y, img, :shader => self.shader)
  end
end
 
 
if $0 == __FILE__
  # ----------------------------------------
  # 使用例
 
  font = Font.new(14)
 
  img = Image.load("bg_960x480.png")
  bg = PipeBg.new(200, 360, 0.0, 0.0)
 
  Window.loop do
    break if Input.keyPush?(K_ESCAPE)
    bg.update(0.0, -0.005)
    bg.draw(-Window.width / 4, 0, img)
 
    s = "#{Window.real_fps.to_i} FPS CPU: #{Window.getLoad.to_i} % x = #{bg.start_x.to_i}"
    Window.drawFont(4, 4, s, font)
  end
#end