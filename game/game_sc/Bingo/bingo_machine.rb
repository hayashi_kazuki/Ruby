require_relative '../Top/top_page'

class BingoManager

  def tyuto
    10.times{|i|
      sleep 0.1
      puts "**"
    }
    print "\n"
    print "                 操作方法から聞きますか？"
    print "\n\n"
    print "             BINGOゲームの遊び方と操作方法は"
    print "\e[33m"
    puts "3"
    print "\e[0m"
    print  "             BINGOの操作方法は"
    print "\e[33m"
    print "4"
    print "\e[0m"
    print "\n" 
    print "\n入力>>"
    select = gets.chomp.upcase
      if select == "3"
        firstExam
      elsif select == "4"
        secondExam
      elsif select == "9"
        top_page = TopPage.new
        top_page.menu
      else
        puts "正しい値を入力してください"
      end
  end # tyuto

  def firstExam #遊び方からの説明
    puts "\n--------------------------------"
    sleep 1
    print "\e[35m"
    puts "ルール１"
    print "\e[0m"
    sleep 2
    puts "\n5×5のマスに１～７５の数字が書かれています。それを消し、早く消し終えた人の勝ちとなるゲームです。"
    sleep 2
    print "\e[35m"
    puts "ルール２"
    print "\e[0m"
    puts "\nタテ・ヨコ・ナナメのいずれか3つのラインを消すことが求められます。 "
    sleep 2
    print "\e[35m"
    puts "ルール３"
    print "\e[0m"
   puts "\n早く上がった人は「ビンゴ！！」といい揃ったことを宣言します。"
   sleep 2
   puts "\n説明は以上です"
   sleep 2
    puts "\n次は操作方法の説明です"
    puts secondExam #操作方法の説明
  end #firstExam

  def secondExam#操作方法の説明
    puts "\n---------------------"
    sleep 2
    puts "\n"
    print "\e[77m"
    print "ゲームの操作方法の説明"
    print "\e[0m"
    sleep 1
    puts ""
    print "操作方法は以下の通りです"
    puts "\n"
    puts "* 一時中断    　5 + enter key"
    puts "* ゲームへ戻る　8 + enter key"
    puts "* ゲーム終了  　9 + emter key"
    sleep 2
    puts "\n---------------------"
    print "\e[36m"
    puts "ゲームをスタートしてよろしいですか？"
    print "\e[0m"
    print "スタートするは"
    print "\e[33m"
    print "  7"
    print "\e[0m"
    puts "\n"
    print  "入力>>"
    gamestart = gets.chomp.upcase

    if gamestart == "7"
      puts playBingo
    else
      puts "正しい数字を入力してください"
    end
  end #secndExam


  def playBingo
     print "\e[33m"
    print "L"
     print "\e[0m"
     print "\e[32m"
    print "E"
     print "\e[0m"
     print "\e[31m"
    print "T"
     print "\e[0m"
     print "\e[38m"
    print "'"
     print "\e[0m"
     print "\e[32m"
    print "S"
     print "\e[0m"
    sleep 2
     print "\e[31m"
    print "B"
     print "\e[0m"
     print "\e[35m"
    print "I"
     print "\e[0m"
     print "\e[34m"
    print "N"
     print "\e[0m"
     print "\e[39m"
    print "G"
     print "\e[0m"
     print "\e[40m"
    print "O"
     print "\e[0m"
    puts "\n"
   puts " ============================="
    puts "||一時中断は　5 + enter key ||     "
    puts "||再開は　    8 + enter key ||      "
    puts "||終了は　    9 + enter key ||      "
   puts " ============================"

    has_stop = false
    thread = Thread.new do
      [*1..75].shuffle.each {|e|
        50.times do |i|
          sleep 0.1
          Thread.stop if has_stop
        end
        puts "\n--------------------------"            
        print "\e[33m"    
        puts  e
        print "\e[0m"
        puts "--------------------------"
     }
    end

   loop do
      selected_menu = gets.chomp.to_i
      case selected_menu
      when 5
        has_stop = true
        puts "STOP!!"
      when 8
        has_stop = false
        thread.run
        puts "RE START!!"
      when 9
        Thread.kill(thread)
        puts "THANKS A LOT!!"
        #exit
        top_page = TopPage.new
        top_page.menu
        break
      else
      end
    end
 end


# 一時中断を行う場合はプレー中に０を押す。再開7
# 終了は９を押して終了
# プレー中の音楽

  def run
     print "\e[31m"
     puts " |======  ====   |＼      |   =======   ======"
     print "\e[0m"
     print "\e[34m"
     puts " |     ||  ||    |  ＼    |  |         |      | "
     print"\e[0m"
     print "\e[35m"
     puts " |======   ||    |    ＼  |  |         |      | "
     print "\e[0m"
     print"\e[36m"
     puts " |     ||  ||    |      ＼|  |   ===== |      |"
     print "\e[0m"
     print"\e[37m"
     puts " |======  ====   |        |   =====||   ======"
     print "\e[0m"
    30.times{|i|
      sleep 0.1
      print "*"
      }
    print "\n"
    puts "                BINGOの世界へようこそ"
    sleep 2
    puts "                チュートリアルを始めます。"
    print "\n"
    print "                チュートリアルを聞きたい方は  "
    print "\e[33m"
    print "  1"
    print "\e[0m"
    print "\n"
    print "                ゲームを始める方は"
    print "\e[33m"
    print "  2"
    print "\e[0m"
    print "\n                終了する場合は"
    print "\e[33m"
    print "9"
    print "\e[0m"
    print "\n\n                入力>>"
    onetwe = gets.chomp.upcase
    if onetwe == "1"
      tyuto
    elsif onetwe == "2"
      playBingo
    elsif onetwe == "9"
        top_page = TopPage.new
        top_page.menu
    else
      puts "正しい番号を入力してください"
    end
    while true
      num = gets.chomp
      case
        when '5' == num
          thread.stop
        when '1' == num
          tyuto
        when '2' == num
          playBingo
        when '3' == num
          firstExam
        when '4' == num
          secondExam
        when '7' == num
          playBingo
        else
      end
    end
  end
end



if __FILE__  == $0
  bingo_manager = BingoManager.new
  bingo_manager.run
end

