require_relative 'const'
require_relative 'Dice'
require_relative '../Top/top_page'


class Game
  def throwdice
    @sai1 = rand(1..6).to_i
    @sai2 = rand(1..6).to_i
    @sum = @sai1 + @sai2
  end  


  def check
    print "  サイコロの合計は偶数(1) OR 奇数(2) ? : "
    
    loop{
      @user = gets.chomp
      if @user == '1' || @user == '2'
        break
      else
        puts "もう一度入力してください"
      end
    }  

    if '1' == @user 
      puts "  あなたは偶数を選びました\n"
      @user = '1'
    elsif '2' == @user
      puts "  あなたは奇数を選びました\n"
    else      
    end
 end


  def result
    
    if @sai1 == 1
      puts Dice::DICE1
    elsif @sai1 == 2
      puts Dice::DICE2
    elsif @sai1 == 3
      puts Dice::DICE3
    elsif @sai1 == 4
      puts Dice::DICE4
    elsif @sai1 == 5
      puts Dice::DICE5
    elsif @sai1 == 6
      puts Dice::DICE6
    end
 
    if @sai2 == 1
      puts Dice::DICE1
    elsif @sai2 == 2
      puts Dice::DICE2
    elsif @sai2 == 3
      puts Dice::DICE3
    elsif @sai2 == 4
      puts Dice::DICE4
    elsif @sai2 == 5
      puts Dice::DICE5
    elsif @sai2 == 6
      puts Dice::DICE6
    end

    puts  "合計は #{@sum}"
    puts "--------------------------"

    if @user == "1" && @sum % 2 == 0
      puts Const::HIT
      @pt += @px
      @win_per += 1
    elsif @user == "1" && @sum % 2 == 1
      puts Const::MISS
      @pt = @pt - @px
    elsif @user == "2" && @sum % 2 == 1
      puts Const::HIT
      @pt = @pt + @px
      @win_per += 1
    else
      puts Const::MISS
      @pt = @pt - @px
    end
  
    print "\n残りポイント : #{@pt}\n"
    p "--------------------------"
    @count += 1
    gets
  end


  def main 
    print "\e[H\e[2J" 
    print "  ゲーム開始!!"
    print "\n  所持ポイント : #{@pt}\n"

    puts "\n  サイコロを二つ振ります。合計が偶数か奇数を当ててください"
    puts "  ゲームを始めますか? (Enter)\n"
    print "  やめる(1) :"
    a = gets.chomp

    if '1' == a
      puts "ゲームを終了しました"
      top
    else
    end

    puts "\n  掛け金を選んでください"
    print "  100 (1) / 50 (2) / 10 (3) = "
    
    num = 0
    loop{
      num = gets.chomp
      if num == '1' || num == '2' || num == '3'
        if num == '1' && @pt < 100
          puts "  ポイントが足りません"
        elsif num == '2' && @pt < 50 
          puts "  ポイントが足りません" 
        else
          break
        end
      else 
        puts "  もう一度入力してください"
      end
    }

    @px = 0
      if '1' == num
        @px = 100
      elsif '2' == num
        @px = 50
      else 
        @px = 10
      end
    puts "  掛け金は : #{@px}"
  end


  def first
    while @pt > 0
       throwdice
       main
       check
       result
    end 
  top
  end


  def game_data
    begin
      puts "------------------"
      puts "  試合数=#{@count} 勝利数=#{@win_per}"
      wp = @win_per.to_f / @count
      wp2 = wp * 100
      puts "  勝率=#{wp2.round(2)}%" 
    rescue
      puts "  結果を表示できません"
    end
      gets
      top
  end

  def data
    @win_per = 0
    @count = 0
  end


  def top
    @pt = 200
    print "\e[H\e[2J"
    print "
   __________________
  |                  |
  |　サ　イ　コ　ロ　|
  |__________________|
 
   0.ゲーム終了
   1.ゲーム開始
   2.ゲーム成績\n  =="
    
    num = 0
    loop{
      num = gets.chomp
      if num == '1' || num == '0' || num == '2'
        break
      else
        puts "  もう一度入力してください"
      end  
    }


    case
    when  num == '1'
      data 
      first
    when '0' == num
      puts "  ゲームを終了しました"
      top_page = TopPage.new
      top_page.menu
    when '2' == num
      game_data
    else
    end
  end 
end

 
if __FILE__  == $0
  start = Game.new
  start.top
end
