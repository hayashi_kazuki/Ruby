#require 'reversi'
require_relative '../saikoro/saikorogame'
require_relative '../BlackJack/BlackJack'
require_relative '../Bingo/bingo_machine'
require_relative '../osero/osero'
require_relative '../minesweeper/mine'

class TopPage
  def menu
    loop do
      print "\e[H\e[2J"
      print "
   1.サイコロ
   2.ブラックジャック
   3.ビンゴ
   4.オセロ
   5.マインスイーパー
   0.ゲームを終了する
   =="
      num = gets.chomp
      print "\e[H\e[2J"
    
      case 
      when num == '1'
        game = Game.new
        game.top
      when num == '2'
        game = BlackJack.new
        game.start        
      when num == '3'
        bingo_manager = BingoManager.new
        bingo_manager.run
      when num == '4'
        osero_manager = OseroManager.new
        begin
          osero_manager.start
        rescue GameQuitError => ex
          # ゲームを終了してメニューを再表示するのでなにもせず
        end
      when num == '5'
        start = MineSweeper.new
        start.menu
      when num == '0'
        #break        
        exit
      end
    end
  end
end


if __FILE__  == $0
  top_page = TopPage.new
  top_page.menu
end 
