require_relative '../Top/top_page'
require_relative '../game_quit_error'
require 'reversi'

module Test
    def input_move
      loop do
        print "#{@color}: "
        @input_move = gets.chomp.split("")
        raise GameQuitError.new if [['q'], ['e','x','i','t']].include? @input_move    
        redo if check_size == :redo
        @input_move[0] = [*:a..:h].index(@input_move[0].to_sym) + 1
        @input_move[1] = @input_move[1].to_i
        printf "\e[#{1}A"; STDOUT.flush
        print "#{" "*9}"
        printf "\e[#{9}D"; STDOUT.flush
        redo if check_valid == :redo
        put_disk(*@input_move)
        break
      end
    end
end

class Reversi::Player::Human
  prepend Test
end

class OseroManager

  def start
    osero_info
    game = Reversi::Game.new
    game.start
    $npc = game.board.status[:white].size
    $player = game.board.status[:black].size
    judge_score
    puts "ENTERキーを押すと戻ります"
    gets.chomp
  end

  def osero_info 
     print "\e[33m"
    puts "  ====   ====== ||    ||  =====  ||      ||        ===="
     print "\e[0m"
     print "\e[34m"
    puts "||    ||   ||   ||    || ||      ||      ||      ||    ||"
     print "\e[0m"
     print "\e[36m"
    puts "||    ||   ||   ||====|| ||----  ||      ||      ||    ||"
     print "\e[0m"
     print "\e[40m"
    puts "||    ||   ||   ||    || ||      ||      ||      ||    ||"
     print "\e[48m"
    puts "  ====     ||   ||    ||  =====   ------   -----   ===="
     print "\e[0m"
   10.times{|i|
     sleep 0.1
     print "*"
    }

    print "\e[33m"
    puts "オセロの世界へようこそ！"
    print "\e[0m"
    print "\n"
    print "オセロゲームの遊び方と操作方法を聞きますか？"
    print "\e[33m"
    print "1"
    print "\e[0m"
    print "\n"
    print "オセロゲームの操作方法だけ聞きますか？"
    print "\e[33m"
    print "2"
    print "\e[0m"
    print "\n"
    print "説明を聞かずゲームを始める"
    print "\e[33m"
    print "7"
    print "\e[0m" 
    print "\nやっぱり、やめる"
    print "\e[33m"
    print "0"
    print "\e[0m"  
    print "\n入力>>"
    choice1 = gets.chomp.upcase
      if choice1 == "1" 
        exam_osero
      elsif choice1 == "2"
        howto_osero
      elsif choice1 == "7"
        start_game
      elsif choice1 == "0"
        top_page = TopPage.new
        top_page.menu
      else
       puts "入力値が正しくありません。確認してください。"
      end 
  end#osero_info
 
  def exam_osero
    10.times{|i|
     sleep 0.2
     puts "*"
    }
    print "\e[34m"
    print "ルール1"
    print "\e[0m"
    puts "石を盤の上に置き、ゲーム終了後に盤に残った石の数が多い方が勝ちのゲームです"
    sleep 3
    print "\e[34m"
    puts "ルール2"
    print "\e[0m"
    puts "違う石と石で挟まれた場合は、挟まれた色に変わります"
    puts "このように。。。。"
    puts "●〇●　　  ●"
    puts "             〇"
    puts "            　●  "
    puts "だと"
    puts "●●●　　 ●"
    puts "            ● "
    puts "              ●"
    puts "と、相手の石を自分の色に変えられて手数を増やせます。"
    puts "挟み方は、タテ・ヨコ・ナナメです"
    sleep 3
    print "\e[34m"
    puts "ルール3"
    print "\e[0m"
    puts "最初は盤の真ん中から石を置くこと。相手の石がある場所から石を置く。"
    10.times{|i|
      sleep 0.2
      puts "*"
     }
    print "\e[33m"
    print "次は操作方法です"
    print "\e[0m"
      howto_osero
  end#exam_osero
 
  def howto_osero
    5.times{|i|
      sleep 0.5
      puts "*"
     }
    puts "操作方法は以下の通りです"
    puts "\n\n"
    sleep 2
    puts "・石を置きたい所を指定します"
    puts "   a b c d e f g h "
    puts " 1"
    puts " 2"
    puts " 3"
    puts " 4"
    puts " 5"
    puts " 6"
    puts " 7"
    puts " 8"
    print "\e[33m"
    puts "横軸のアルファベットと縦軸の数字で場所指定を行います。"
    print "\e[0m"
    puts "\n"
    sleep 3
    puts "・置けない個所を選ぶとエラー表示がされます。"
    puts "全て置き終わると終了します。"
    puts "\n"
    sleep 2
    puts "ゲームをスタートします。よろしいですか？"
    puts "\n"
    puts "ゲームをスタートする"
    print "\e[33m"
    puts "7"
    print "\e[0m"
    print "入力>>"
    choice2 = gets.chomp.upcase
      if choice2 == "7" || choice2 == "７"
        start_game
      else 
        puts "入力した値を確認してください"
      end
  end#howto_osero


  def start_game 
    #デモの表示と初期設定
    Reversi.configure do |config|
      config.disk_color_b = 'cyan'
      config.disk_b = 'O'
      config.disk_w = 'O'
      config.progress = false#trueで下のREVERSI.～endまで消すとオートモード
    end
     
      puts "途中でやめるときはqを押してください"
     #ゲーム開始
     Reversi.configure do |config|
       config.player_b = Reversi::Player::Human
  
     end
  
  end#start_game

  has_end = false
  def finish
    top_page
  end




  #ジャッジする
  def judge_score
    if $player > $npc
      puts "YOU WIN!!!"
    
      #戻り画面
    elsif $player < $npc
      puts "YOU LOST!!!"
    else
      puts "DORRO"
    end
  end#judge_score

end#class


if $0 == __FILE__
  osero_infos = OseroManager.new
  osero_infos.start
end
