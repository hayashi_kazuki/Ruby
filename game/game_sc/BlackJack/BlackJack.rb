require_relative 'Constant'
require_relative '../Top/top_page'

class BlackJack

  def first
    while @pt > 0
      @round += 1
      deal_cards
    end
   start
  end

  def start
    @pt = 200
    print "
    ______________
   |              |
   |  Black Jack  | 
   |______________|

    0.ゲーム終了
    1.ゲーム開始
    == "
    num = 0
    loop{
      num = gets.chomp
      if  num == '1' || num == '0'
        break
      else
        puts "  もう一度入力してください"
    end      
}  

    case
    when '1' == num
      @round = 0
      first
    when '0' == num
      puts "  ゲームを終了しました"
      #exit
      top_page = TopPage.new
      top_page.menu
    else
      exit
    end
  end


  # カードを配る
  def deal_cards
    @dealer = []
    @player = []

    puts "---------------------"
    #print "\e[H\e[2J"
    puts "
  カードの合計点が21点を超えないように、相手より高い点数を目指してください。
  ゲームを始めますか?(Enter)
  ゲームをやめる(1) "
    a = gets.chomp
    if a == '1'
      print "\e[H\e[2J"
      puts "  ゲームを終了しました"
      start
    else
    end

    @i = 0
    print "\e[H\e[2J"
    puts "  ゲーム開始!!"
    print "  ラウンド#{@round}    ポイント #{@pt}\n"
    puts "-------------------"

    puts "  掛け金を選んでください  "
    print "(1) 100 / (2) 50 / (3) 10 = "
    num = 0
    loop{
      num = gets.chomp
      if num == '1' || num == '2' || num == '3'
        if num == '1' && @pt < 100
          puts "  ポイントが足りません"
        elsif num == '2' && @pt < 50
          puts "  ポイントが足りません"
        else
          break
        end
      else 
        puts "  もう一度入力してください"
      end
}

    @px = 0  
    if num == '1'
      @px = 100
    elsif num == '2'
      @px = 50
    else
      @px = 10
    end

    puts " 掛け金は : #{@px}"

    @player << rand(1..11)
    if @player[0] == 11
      @player << rand(1..10)
    else 
      @player << rand(1..11)
    end
    print "  \nあなたのカード "

    print "
「
   "
    print   @player[0]
    print "　
      」"
    print "          合計= #{@player.inject(:+)}\n"
    print "
「
   "
    print   @player[1]
    print "　
      」\n"


    @dealer << rand(1..11)
    if @dealer[0] == 11
      @dealer << rand(1..10)
    else
      @dealer << rand(1..11)
    end

    
    print "------------------"

    print "
「
   "
    print   @dealer[0]
    print "　
      」"
    print "  ディーラーのアップカード"

    print "
「
   "
    print   "?"
    print "　
      」"
    print "  ホールカード"
    puts ""
    #カードの判定に移動
    first_check
  end



  def first_check
    pi = @player.inject(:+)
    di = @dealer.inject(:+)

    if di < 21 && pi == 21
      print "\n------------------\n\n"
      puts Constant::PLAY
      puts  #{pi} #{di}
      win
    elsif di == 21 && pi == 21
      print "\n------------------\n\n"
      puts Constant::LOSE
      puts  #{pi} #{di}
      lose
    else
      cards_add
    end
  end



  #最後カードの判定
  def last_check
    pi = @player.inject(:+)
    di = @dealer.inject(:+)

    puts "       Result"
    puts "  あなたのカードは   #{pi}　 #{@player}"
    puts "  相手のカードは　   #{di}   #{@dealer}"
    puts ""


    if di == 21 && pi == 21
      puts Constant::DEAL
      lose  
    elsif di < 21 && pi == 21
      puts Constant::PLAY
      win
    elsif  di == 21 && pi < 21
      puts Constant::DEAL
      lose
    elsif di == pi
      puts Constant::DROW
      drow
    elsif di < pi
      puts Constant::WIN
      win
    elsif di > pi
      puts Constant::LOSE
      lose
    else
    end
  end


  def cards_add
    while @player.inject(:+) < 21
      print "\n  カードを引きますか?(1.Yes / 2.No) = "
      
      add = 0
      loop{
        add = gets.chomp
          if add == '1' || add == '2'
            break
          else 
            puts "  もう一度入力してください"  
          end
      }

      if add == "1"
        puts "== YES"
        @player.push(rand(1..11))
        print "  あなたのカード合計は #{@player.inject(:+)}　 #{@player}\n"
      else
        puts  "== NO"
        break
      end
    end

    puts "----------------------"

    if @player.inject(:+) > 21
      puts "  21を越えました。　あなたの負け"
      lose
    end
      last_check
  end


  def win
    @i += 2
    pt_cal
  end

  def lose
    @i += 1
    pt_cal
  end

  def drow
    @i = 0
    pt_cal
  end

  def pt_cal
    if @i == 2
      @pt = @pt + @px
    elsif @i == 1
      @pt = @pt - @px
    else
    end

  puts "-------------------"

  puts "  残りポイント : #{@pt}"    

  first

  end
end


if __FILE__  == $0
game = BlackJack.new
game.start
end
